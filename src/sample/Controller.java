package sample;

import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;

import java.net.URL;
import java.util.ResourceBundle;

public class Controller implements Initializable {
    @FXML
    private TextField nome;
    @FXML
    private TextField email;
    @FXML
    private TextField senha;
    @FXML
    private TextField confirmaSenha;
    @FXML
    private Button botao;

    private UsuarioService service;

    @Override
    public void initialize(URL url, ResourceBundle resource) {
        service = UsuarioService.getNewInstance();
    }

    private void dados(Usuario user) {
        user.setNome(nome.getText());
        user.setEmail(email.getText());
        user.setSenha(senha.getText());
        user.setConfirmaSenha(confirmaSenha.getText());
    }

    public void salvar() {
        Usuario user = new Usuario();
        dados(user);
        service.salvar(user);
    }
}
