package sample;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

public class Main extends Application {

    @Override
    public void start(Stage primaryStage) throws Exception{
        Parent root = FXMLLoader.load(getClass().getResource("cadastro.fxml"));
        primaryStage.setTitle("Cadastro de Usu�rio | Eco Re�se");
        primaryStage.setScene(new Scene(root));
        primaryStage.show();
    }
    
    private static final EntityManagerFactory entity = Persistence.createEntityManagerFactory("usuarios");

    public static void main(String[] args) {
    	launch(args);
    	inserirDados();
    }
    
    public static void inserirDados() {        
        EntityManager em = entity.createEntityManager();
        Usuario user = new Usuario();
        
        try {
        	em.getTransaction().begin();
        	em.persist(usuarios);
        	em.getTransaction().commit();
        } catch (Exception e) {
        	em.getTransaction().rollback();
        	System.out.println(("insert: " + e.getMessage()));
        } finally {
        	em.close();
        }
    }
    
}
