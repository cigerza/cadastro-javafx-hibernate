package sample;

public interface UsuarioService {
    public void salvar(Usuario user);
    public Usuario buscaPorId(int id);
    public void atualizar(Usuario user);

    public static UsuarioService getNewInstance() {
        return new UsuarioDBService();
    }
}
